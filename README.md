---
title: "README"
output: html_document
---

There are two modes when using `rdocker` package:

* Development mode -- used by application dvelopers locally
* User mode -- end-product usage by end-user

## 1. DEVELOPMENT MODE

### 1.1 Prerequisties

* `docker` is installed
* `docker` can be executed without `sudo`
* Local `R` installation
* Local `R` packages are all up-to-date
* Local `RStudio` installation (_optional_)

### 1.2 Docker Image Setup (_one time task_)

Application image `redmode/r-base` is based on latest `ubuntu` image and has minimal set of packages installed, including `rdocker`.

```{r}
docker pull ubuntu
docker run -it ubuntu /bin/bash
```

In order to have latest version of `R` one need add `deb http://cran.rstudio.com/bin/linux/ubuntu trusty/` line to ` /etc/apt/sources.list` file. 

Then run:

```{r}
sudo apt-get update
sudo apt-get upgrade

sudo apt-get install r-base r-base-dev
sudo apt-get -y build-dep libcurl4-gnutls-dev
sudo apt-get -y install libcurl4-gnutls-dev
```

Next portion should be done in `R` as root, i.e. run `sudo R` and execute:

```{r}
install.packages(c("RCurl", "httr", "devtools"))
devtools::with_libpaths("/usr/lib/R/library",
                        devtools::install_bitbucket(repo = "redmode/rdocker",
                        auth_user = "<BITBUCKET_USER>", password = "<BITBUCKET_PASSWORD>"))
devtools::with_libpaths("/usr/lib/R/library", update.packages())                        
```

After that modified container should be commited and pushed to the repo.

```{r}
docker commit <CONTAINER_ID> redmode/r-base
docker login -u '<DOCKER_USER>' -p '<DOCKER_PASSWORD>' -e '<DOCKER_MAIL>' && docker push redmode/r-base
```

*NOTE*: `<CONTAINER_ID>` can be retrieved with `docker images` while container is running.

Now `redmode/r-base` can be used as base container for `rdocker` projects.

### 1.3 Project Creation

Assuming developer is working on the local machine he has to have `rdocker` installed.

```{r}
install.packages(c("RCurl", "httr", "devtools"))
devtools::with_libpaths("/usr/lib/R/library",
                        devtools::install_bitbucket(repo = "redmode/rdocker",
                        auth_user = "<BITBUCKET_USER>", password = "<BITBUCKET_PASSWORD>"))
```

Next step is project creation:

```{r}
rdocker::create_project("demo_rdocker2", project.path = "/home/ales/projects")
```

Now any changes to the project folder can be done: new files added, packages installed, etc. For testing `titanic` project fron kaggle was used. Some packages with complex dependencies and suggestions were used:

```{r}
install.packages(c("randomForest", "caret", "dplyr"))
```

### 1.4 Resume Project Development (_locally_)

While working on project locally developer can resume his work with this commands:

```{r}
rm(list = ls(all = TRUE))
packrat::init("/home/ales/projects/demo_rdocker2")
```

In case of `titanic` project this line can be added:

```{r}
source("main.R")
```

### 1.5 Pushing Project to the Docker Hub

At any point, i.e. with current workspace project can be pushed to the hub:

```{r}
rdocker::push_project(project.path = "/home/ales/projects/demo_rdocker2",
                      image = "redmode/r-base",
                      repo = "redmode/rdocker",
                      account = list(user = "<DOCKER-USER>", password = "<DOCKER-PASSWORD>", email = "<DOCKER-EMAIL>"))
```

Here `image = "redmode/r-base"` is a base image with `R`, `rdocker`, etc. installes (see section 1.2).


### 1.6 Resume Project Development (_on another machine_)

Working on the project can be resumed on another machine. It requires that project was pushed to the hub previously.

```{r}
rdocker::pull_project(title = "demo_rdocker",
                      project.path = "/home/ales/projects/demo_rdocker_restored",
                      repo = "redmode/rdocker",
                      account = list(user = "<DOCKER-USER>", password = "<DOCKER-PASSWORD>", email = "<DOCKER-EMAIL>"))
```

This approach can also be used to clone the project.


## 2. USER MODE

## 2.1 Prerequisites

* Docker is installed

## 2.2 Deliverables

User should be provided with following credentials:

* Name of the docker image at hub, i.e. `redmode/rdocker`
* Project title, i.e. `demo_rdocker2`

## 2.3 Project Loading

It can be tested at AWS EC2 or locally:

```{r}
ssh -i ~/.ssh/Rdocker.pem ubuntu@ec2-54-153-47-146.us-west-1.compute.amazonaws.com
```

Initially, user should pull image and run container:

```{r}
docker pull redmode/rdocker
docker run -it redmode/rdocker R
```

Then in `R` console project should be loaded:

```{r}
rdocker::load_project("demo_rdocker2")
```

It can give warnings, but should work. To test workspace one can check `model_rf` variable.

